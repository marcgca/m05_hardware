### Exercicis Màquines virtuals


##### Exercici 0

* Mostreu informació dels paquets que s'instal·len amb el grup de virtualització (a Fedora):

  ```bash
  dnf groupinfo virtualization
  ```

* Instal·leu els paquets opcionals, que ens permetra jugar amb les *virt tools*.

  ```bash
  dnf -y groupinstall --with-optional virtualization
  ```

* Si volem que qualsevol usuari (no root) pugui utilitzar virt-manager, com ho podem fer?

  * A l'opció `auth_unix_rw= "none"` del fitxer `/etc/libvirt/libvirtd.conf` podem autoritzar que qualsevol usuari ho pugui utilitzar.

* Amb l'ordre `qemu-kvm` volem arrencar el nostre GRUB i seleccionar una entrada que no utilitzi la partició que conté el sistema de fitxers de l'amfitrió. Afegiu la RAM que sigui necessària.

  ```bash
  sudo qemu-kvm -m 1G /dev/sda
  ```

* A quin directori s'emmagatzemmen els discos virtuals?

  * Es troba al directori `/var/lib/libvirt/images`.

* A quin directori s'emmagatzemem les característiques de les *VM*?

  * Al directori `/etc/libvirt/qemu`.

*  I de les xarxes?

  * Al directori `/var/lib/libvirt/network`.

* Si intentem editar *a pèl* un dels fitxer XML que contenen les configuracions *hardware* de les MV que es troben al directori anterior veurem un warning alertant-nos de quina és la manera dient de fer canvis en aquest fitxer. Quina és aquesta forma?

  * Ens indica que ho fem mitjançant l'ordre:

    ```bash
    virsh edit nom_màquina
    ```

##### Exercici 1

Munteu un servidor ssh amb una màquina virtual. Per a això:

* Decidiu amb quin tipus de xarxa serà més fàcil (o possible).
  * La millor opció és un bridge.
* Configureu el *guest* amb el tipus de xarxa escollit per a que pugui funcionar com a servidor ssh.
  * Li assignem una ip estàtica per sapiguer sempre on ens connectem. Ho fem al fitxer `/etc/sysconfig/network-scripts/ifcfg-enp2s0` o nom d'interfície.
  * Ho habilitem amb un `systemctl enable sshd.service`.
* Intenteu fer un accés ssh des d'un ordinador extern.
  * Es pot fer.

##### Exercici 2

Munteu un servidor ftp amb una màquina virtual. Per a això:

* Decidiu amb quin tipus de xarxa serà més fàcil (o possible).
  * La millor opció és un bridge.
* Configureu el *guest* amb el tipus de xarxa escollit per a que pugui funcionar com a *servidor ftp*:  `vsftp`. Activeu el dimoni.
  * Li instal·lem el paquet vsftp, després l'activem amb un `systemctl enable vsftpd`.
* Intenteu fer un accés ftp des d'un ordinador extern. Utilitzeu un *client ftp*: `ftp`. Nota: l'usuari que fareu servir com a login, `anonymous`, al servidor te com a nom `ftp`, comproveu que existeix al fitxer `/etc/passwd` i feu un cop d'ull al seu directori personal.
  * Accedim amb el client ftp i la ip del servidor. Entrem com `anonymous` i trobem el que hi ha a la carpeta de publicació `/var/ftp/`.

##### Exercici 3

Conversió entre plantilles de VM. A partir d'una imatge de VirtualBox `*.ova` crearem una VM, convertint prèviament al format `*.qcow2`.

Utilitzarem el fitxer `Fedora24_CiscoPakcetTracer_DocumentacioCisco_6_9_16.ova` que es troba tant a public. Podem però fer servir udpcast per distribuir per broadcast el fitxer i no congestionar l'ample de banda.

Des de l'ordinador que ofereix el fitxer

```
udp-sender --file Fedora24_CiscoPakcetTracer_DocumentacioCisco_6_9_16.ova
```

Des dels receptors:

```
udp-receiver --file Fedora24_CiscoPakcetTracer_DocumentacioCisco_6_9_16.ova
```

* El primer pas serà *extreure* la imatge de disk continguda al fitxer `Linkat.ova` amb l'ordre `tar`.

  ```
  tar -xf Fedora24_CiscoPakcetTracer_DocumentacioCisco_6_9_16.ova
  ```

* Que representen cadascun dels fitxers trobats?

    ```bash
    [isx47797439@j08 ~]$ ll Fedora24*
    -rw-r--r--. 1 isx47797439 hisx1 2813056000 May 17 09:10 Fedora24_CiscoPacketTracer_DocumentacioCisco_6_9_16.ova
    -rw-------. 1 isx47797439 hisx1 2813042176 Sep  6  2016 Fedora24-disk1.vmdk
    -rw-------. 1 isx47797439 hisx1      12797 Sep  6  2016 Fedora24.ovf
    ```

    * El fitxer **.vmdk** és el disc virtual.
    * El fitxer **.ovf** és qui conté la configuració de la màquina virtual.

* Quina utilitat QEMU ens permet fer conversions d'imatges de disc?

    * `qemu-img convert -f ext1 -O ext2 fitxer fitxer_nou` 

* Comprova si està instal·lada aquesta eina i instal·la-la si fos necessari per després fer la conversió a format `qcow2`.

  ```bash
  [isx47797439@j08 ~]$ rpm -ql "qemu-img"
  /usr/bin/qemu-img # Sí que el tenim instal·lat
  /usr/bin/qemu-io
  /usr/bin/qemu-nbd
  /usr/lib/.build-id
  /usr/lib/.build-id/77
  /usr/lib/.build-id/77/14d58659d70f77c69276b809009e9ef7b15a8d
  /usr/lib/.build-id/8c
  /usr/lib/.build-id/8c/ad1eef6fb934d2a1f4c7e0f236c497c8cbd199
  /usr/lib/.build-id/db
  /usr/lib/.build-id/db/e48501fca3274cf0740b1578d949599b331d74
  /usr/share/man/man1/qemu-img.1.gz
  /usr/share/man/man8/qemu-nbd.8.gz
  ```

* Utilitzant l'eina `virt-install` instal·leu la imatge que acabeu de crear (i que hauria d'estar al directori habitual d'imatges) amb les següents característiques:
    * nom: F24

    * ram: 1024 (MB)

    * sist. op.: linux

      ```bash
      virt-install --name F24 --memory 1024 --os-variant fedora24
      ```

##### Exercici 4

Quina ordre del paquet `libguestfs-tools` ens permet, entre d'altres coses:

* *Resetejar* una màquina virtual de manera que es pugui clonar una VM sense configuracions posteriors. Per exemple, eliminant claus de host SSH, configuracions persistents de la xarxa o comptes d'usuaris

  ```bash
  virt-sysprep -d nom_màquina
  #
  virt-sysprep -a nom_disc.img
  ```

* *Customitzar* una màquina virtual de manera que es pugui clonar una VM amb una configuració concreta. Per exemple, afegint claus SSH, comptes d'usuaris, etc.

  ```bash
  # Primer hauriem de clonar la màquina i després customizar-la
  virt-clone --original nom_màquina --name nom_nova_màquina --auto-clone
  virt-customization -d nom_nova_màquina --instal "nom_paquet" --run-command 'comanda+arguments'
  ```

(Per fer qualsevol de les operacions anteriors s'ha de apagar la VM primer i si ens interessa preservar l'estat inicial del guests'haurà de fer algun tipus de clonatge primer)

Quina ordre també del paquet `libguestfs-tools` ens permet alliberar cert espai no utilitzat del disc imatge? 

```bash
# És recomenable fer un clone abans, per si ens fa malbé el disc
virt-sparsify --in-place disc.img
```

##### Exercici 5 (virsh)

* A quin paquet es troba *virsh*?

  * Es troba al paquet `fence-agents-virsh-x86_64`.

* Llistar **totes** les màquines virtuals (les engegades i les que no ho estan)

  * Amb la opció `list --all`.

    ```bash
    virsh list --all
    ```

* Arrenca una VM que està apagada:

  * Amb la opció `start`.

    ```bash
    virsh start nom_màquina
    ```

* Mostrem ara només les VM's que estiguin engegades

  * Amb la opció `list`.

    ```bash
    virsh list
    ```

* Amb quina eina es podrà mostrar la consola gràfica d'una VM concreta? El protocol utilitzat per accedir a aquesta consola gràfica és VNC o SPICE. (Hint: virt-whatever...)

  * Amb el visor `virt-viewer`.

    ```bash
    virt-viewer nom_màquina
    ```

* Com s'apaga una VM? I com es reinicia? I si a l'apagar no ens fa gaire cas? com hiverna? que es guarda al fitxer d'hibernació? i com desperta? com es suspen? com es reprèn? I si vull saber l'estat de només una VM concreta? I si vull tenir tota la info d'una VM concreta? I si vull eliminar una VM? I si vull eliminar-la carregant-me el disc dur virtual?

  ```bash
  # Apaguem VM
  virsh shutdown nom_màquina
  # Reiniciem VM
  virsh reboot nom_màquina
  # Forçem el shutdown si la màquina no ens fa cas perque està penjada
  virsh destroy nom_màquina
  # Hivernem VM, que guarda el que estem fent en aquell moment
  virsh save nom_màquina nom_fitxer
  # "Despertem" la VM de la hibernació
  virsh restore nom_fitxer
  # Suspenem la VM
  virsh suspend nom_màquina
  # "Despertem" la VM
  virsh resume nom_màquina
  # Sapiguer l'estat d'una VM concreta
  virsh domstate nom_màquina
  # Sapiguer tota la informació d'una VM
  virsh dominfo nom_màquina
  # Eliminar una VM
  virsh undefine nom_màquina
  # Eliminar una VM carregant-nos el disc dur virtual
  virsh undefine nom_màquina --remove-all-storage
  ```

##### Exercici 6

Volem connectar-nos a una consola de text d'una VM amb virsh, però per a fer això no només necessito descobrir quina és l'opció per a virsh sinó també configurar algun fitxer i algun *tweak* extra a la VM.

Configureu la VM per a poder permetre l'acces a una consola de text.

OBS: Per sortir de la sessió s'ha de fer \<Ctrl>\+\<Alt. Gr.\>+\<]\>

* L'ordre per accedir a una consola de la VM és amb la opció `console`

  ```bash
  virsh console nom_màquina
  ```

* Tot i això, necessitem tindre-ho habilitat a la VM a la línia del kernel de l'entrada del Grub indicant `console=ttyS0` manualment al arrencar o habilitant-ho amb un *systemctl* .

  ```bash
  # A la VM
  systemctl enable serial-getty@ttyS0.service
  ```

#####  Extras

Habitualment les eines que corren per sobre de `libvirt`, com poden ser `virt-manager` o `virsh`, necessiten comptes d'usuari amb privilegis: root o usuaris quepuguin fer servir root. Si volem però tenir alguns usuaris diferents d'aquests amb privilegis per a aquests casos existeixen certes solucions; [en aquest enllaç](https://major.io/2015/04/11/run-virsh-and-access-libvirt-as-a-regular-user/) podem trobar una possibilitat.  




