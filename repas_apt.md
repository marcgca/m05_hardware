# Gestor de paquets e instal·lació a Debian

## Apt-get

* El fitxer de configuración dels repositoris a Debian es troba a `/etc/apt/sources.list`

* La informació que conté aquest arxiu:

  ```shell
  deb http://site.example.com/debian distribution component1 component2 component3
  ```

  * `deb` o `deb-src` indica el tipus d'arxiu: binaris o fonts.
  * La URL d'on es troba el reopositori
  * El nom de la versió de Debian (*jessie, stretch, buster, sid*) o la classe de la versió (*oldstable, stable, testing, unstable*). El nom de la versió és estàtica, és a dir, només es refereix a aquella versió en particular. Al contrari, la classe és dinàmica, ja que pot ser qualsevol versió de Debian.
  * Per últim ens trobem amb els complements, que poden ser:
    * `main`: És la única completament lliure.
    * `contrib`:  El còdi pot ser lliure, però pot tindre dependències de software no lliures.
    * `non-free`: No és lliure.

* Per actualitzar els paquets, primer haurem de fer l'`update` i després l'`upgrade` , per separat. Al contrari que a Fedora, que fem directament el segon pas, a Debian necessitem fer els dos passos.

  * Ho podem fer de forma interactiva amb el programa `aptitude`
    * Amb `/paquet` podem cercar el paquet que busquem.
    * Per seleccionar el resultat que volguem, en cas de que surtin més d'1, ho fem amb `+`. Un cop seleccionat, veurem que apareixerà la lletra `i`.
    * A continuació haurem d'apretar dos cops  `g `. El primer per resoldre les dependències i el segon per confirmar l'instal·lació.

* Tenim grups de paquets com a Fedora. A Debian s'anomenen *task* i la comanda per instal·lar-les és:

  ```bash
  tasksel
  #
  apt-get install nom_de_la_tasca^
  ```

  

  * Molt important ficar el caracter *^* després del nom per indicar que es una task.

* Tot i que no hi han gaires grups, els podem llistar amb:

  ```bash
  tasksel --list-tasks
  ```

* Amb `dpkg` podem mostrar el contingut d'un paquet amb l'opció `-L`. Amb `-l` mostrem tots els paquets instal·lats al sistema.

  ```bash
  # Contingut del paquet especificat
  dpkg -L xterm
  # Paquets instal·lats al sistema.
  dpkg -l
  ```

* Per cercar un paquet fem servir `apt-cache|apt search paquet`. Si volem que només ho busqui pel nom i no en la descripció o en el contingut del paquet ho farem amb  `--names-only`.