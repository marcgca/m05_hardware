# Comprimits amb tar

* Per a crear un fitxer tar seguirem el següent esquema:

  ```bash
  tar -cvf nom_comprimit.tar /destinació/
  ```

  * `c (create)`:  Crea el fitxer tar
  * `v (verbose)`: Mostra informació
  * `f (file)`: posa el nom al comprimit

* Podem especificar el tipus de comprimit, ja sigui `gzip`, `bzip2`, `xz`:

  ```bash
  # Per gzip
  tar -czvf comprimit.tar.gz
  # Per bzip2
  tar -cjvf comprimit.tar.bz
  # Per xz
  tar -cJvf comprimit.tar.xz
  ```

* Per descomprimir canviem la `c` per `x`:

  ```bash
  tar -xzvf comprimit.tar.gz
  ```

* Amb l'opció `-t`llistem el contingut d'un comprimit sense necessitat de descomprimir:

  ```bash
  tar -tf comprimit.tar.gz
  ```

* Si volem descomprimir amb dos passos, primer haurem de descomprimir el tipus de fitxer:

  ```bash
  # Per gzip
  gunzip postgresql-documentacio.tar.gz
  # Per bzip2
  bunzip postgresql-documentacio.tar.bz
  # Per xz
  unxz postgresql-documentacio.tar.xz
  
  tar xvf postgresql-documentacio.ta 
  ```

* Podem indicar el nivell de compressió que volem, sent de menor a major i de més a menys temps de compressió respectivament

  ```bash
  # Per gzip
  -1/-9 # Min/max compressió
  --fast/--best # Més ràpid, menys compressió / Més lent, més compressió
  # Per bzip2
  -1/-9 # Min/max compressió
  --fast/--best # Més ràpid, menys compressió / Més lent, més compressió
  # Per xz
  -0/-9 # Menys/més compressió, el valor per defecte és -6
  ```

  