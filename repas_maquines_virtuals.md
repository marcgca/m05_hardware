## Introducció a les màquines virtuals

Les màquines virtuals són màquines que, com el seu propi nom indica, són simulades per software fent servir el hardware real.

![estrucura MV](https://gitlab.com/hisx-m05-fonaments-maquinari/uf3-public/raw/master/1-Maquines-Virtuals/Kernel-based_Virtual_Machine.png)

![Estructura MV](https://gitlab.com/hisx-m05-fonaments-maquinari/uf3-public/raw/master/1-Maquines-Virtuals/pila_KVM_QEMU.png)

Com podem observar, la màquina real s'anomena ***host*** (amfitrió) i la virtualitzada ***guest*** (invitat).

No hi ha una única forma de virtualització, entre les més conegudes hi han les següents:

```
                          +--------------------------------------+
                          |         MÀQUINES VIRTUALS            |
                          +-+----------+--------------+-----+----+
                            |          |              |     |
                            |          |              |     +----------------------------------+
        +-------------------+          |              |                                        |
        |                              |              |                                        |
        |                              |              +-------------------+                    |
        |                              |                                  |                    |
        |                          +---+---+                              |                    |
        |                          | Linux |                              |                    |
        |                          ++----+-+                              |                    |
        |                           |    |                                |                    |
        |                   +-------+    +--------+                       |                    |
        |                   |                     |                       |                    |
 +------v------+         +--v--+               +--v--+             +------v------+     +-------v-----+
 | Virtual Box |         | KVM¹|               | Xen |             |   VMware    |     |   Hyper_V   |
 +-------------+         +-----+               +-----+             +-------------+     +-------------+
 Oracle, lliure?     Kernel based               Cytrix            Programari privatiu  Microsoft, privatiu
 toy?                Lliure, Red Hat          vdi like isard      Líder petita i       poca quota mercat
                            +                                     mitjana empresa
                            |
                            | libvirt²
        +------------------------------------------+
        |                   |                      |
        |                   |                      |
+-------+------+        +---+---+            +-----+------+
| virt_manager |        | oVirt |            | Open Stack |
+--------------+        +-------+            +------------+
Interfaz gràfica       Solució petita i      Líder empreses grans (NASA, +500)
gestió VM's            mitjana empresa       Orientat a cloud (Amazon WS, OVH ...)
                                             IaaS³
```

A Linux el més "comú" és el KVM (Kernel-Based Virtual Machine) que utilitza la llibreria *libvirt*.

![llibreria libvirt](https://gitlab.com/hisx-m05-fonaments-maquinari/uf3-public/raw/master/1-Maquines-Virtuals/libvirt_support.png)



## Instal·lació de KVM a GNU/LINUX

Primer de tot hem de sapiguer si la nostre cpu **permet** la virtualització:

```bash
# Trobem aquesta informació a /proc/cpuinfo
egrep "vmx|svm" /proc/cpuinfo 
# VMX és per a cpu's d'Intel i SVM per a cpu's d'AMD 
```

* Si al executar el grep ens retorna un resultat es que sí admet la virtualització, però pot ser que estigui desactivada a la bios. Si és aquest cas, al encendre el Linux ens ho notifica, sol hem d'habilitar-ho a la bios.

A Fedora, el grup que permet la virtualització és `@virtualization`:

```bash
dnf -y install @virtualization
```

Si volem que qualsevol usuari (no root) pugui utilitzar virt-manager:

* L'opció `auth_unix_rw= "none"` del fitxer `/etc/libvirt/libvirtd.conf`.

## Contingut VM

Si descomprimim una VM ens trobarem amb 2 fitxers:

```bash
[isx47797439@j08 ~]$ ll Fedora24*
-rw-r--r--. 1 isx47797439 hisx1 2813056000 May 17 09:10 Fedora24_CiscoPacketTracer_DocumentacioCisco_6_9_16.ova
-rw-------. 1 isx47797439 hisx1 2813042176 Sep  6  2016 Fedora24-disk1.vmdk
-rw-------. 1 isx47797439 hisx1      12797 Sep  6  2016 Fedora24.ovf
```

* El fitxer **.vmdk** és el disc virtual.
* El fitxer **.ovf** conté la configuració de la màquina virtual.

El direcori on es troben emmagatzemats els discos virtuals és:

* `/var/lib/libvirt/images`

El directori on es troben les característiques de les VM és:

* `/etc/libvirt/qemu`.

El directori on es troben les xarxes és:

* `/var/lib/libvirt/network`.

## Execució de VM

Es poden executar tant amb un programa de forma gràfica o mitjançant comandes. Ambdues es mostren amb hipervisors (en cas de trobar-nos en una sessió gràfica o que volguem controlar-la de forma gràfica).

### Programa gràfic

Els programes gràfics per VM permeten crear/modificar les màquines virtuals a nivell de hardware, creació/edició de xarxes...

A Linux, per defecte, el programa utilitzat és el `virt manager`. Hi han altres com el `virtual box`.

![creació VM a virtual manager](https://gitlab.com/hisx-m05-fonaments-maquinari/uf3-public/raw/master/1-Maquines-Virtuals/virt-manager-pas1.png)



### Comandes per consola

Una de les comandes que més utilitzarem per gestionar màquines virtuals en entorns de consola és `virsh`.

```bash
# Mostrem totes les VM / només les engegades
virsh list --all
virsh list
# Arrenquem una VM que està apagada
virsh start nom_màquina
# Apaguem VM
virsh shutdown nom_màquina
# Reiniciem VM
virsh reboot nom_màquina
# Forçem el shutdown si la màquina no ens fa cas perque està penjada
virsh destroy nom_màquina
# Hivernem VM, que guarda el que estem fent en aquell moment
virsh save nom_màquina nom_fitxer
# "Despertem" la VM de la hibernació
virsh restore nom_fitxer
# Suspenem la VM
virsh suspend nom_màquina
# "Despertem" la VM
virsh resume nom_màquina
# Sapiguer l'estat d'una VM concreta
virsh domstate nom_màquina
# Sapiguer tota la informació d'una VM
virsh dominfo nom_màquina
# Eliminar una VM
virsh undefine nom_màquina
# Eliminar una VM carregant-nos el disc dur virtual
virsh undefine nom_màquina --remove-all-storage
# Editem una VM
virsh edit nom_màquina
# Hipervisor
virt-viewer nom_màquina
```

* Per accedir a una VM com a consola ho fem amb l'opció **console**, però hem d'habilitar-ho a la màquina virtual primer:

  ```bash
  # A la VM
  systemctl enable serial-getty@ttyS0.service
  # A la màquina real
  virsh console nom_màquina
  ```

## Tipus de xarxes

### NAT

Es l'opció per defecte, el switch virtual opera en mode NAT (Network Address Translation), concretament IP masquerading.

Permet a les màquines virtuals connectar-se cap a l'exterior utilitzant la IP de l'amfitrió, però per defecte les màquines de l'exterior no veuen a la VM.

![mode NAT](https://gitlab.com/hisx-m05-fonaments-maquinari/uf3-public/raw/master/1-Maquines-Virtuals/tipus_de_xarxes/vn-04-hostwithnatswitch.png)

### Bridge

En aquest mode les VM es troben dins la mateixa xarxa física que l'amfitrió, és a dir, és com s'hi hi estigués de forma física. Totes les màquines la poden veure.

Aquest mode treballa a la capa 2 del model de xarxa OSI.

![mode bridge](https://gitlab.com/hisx-m05-fonaments-maquinari/uf3-public/raw/master/1-Maquines-Virtuals/tipus_de_xarxes/vn-Bridged-Mode-Diagram.png)

### Isolated (aïllades)

En aquest mode les VM que es trobin en aquesta xarxa estaràn totalment aïllades. Només es podràn veure les VM que estiguin a la mateixa xarxa isolated. Totes les físiques no les podràn veure, ni tant sols l'amfitrió.

* Tot i això, els noms de DNS encara es resolen, però no funciona cap altre comunicació.

![xarxa isolated](https://gitlab.com/hisx-m05-fonaments-maquinari/uf3-public/raw/master/1-Maquines-Virtuals/tipus_de_xarxes/vn-07-isolated-switch.png)

