### Exercicis Màquines virtuals

Introducció: creació de Màquines Virtuals

##### Exercici 1

Utilitzeu la xuleta d'[instal·lació de kvm](instal_kvm.md) per poder virtualitzar amb KVM i posteriorment creeu amb *virt manager* alguna màquina virtual a partir d'alguna _*.iso_ de les que es pot trobar a *gandhi* consensuada amb el professor.

##### Exercici 2

El programa *virt-builder* ens permet construir VM d'una manera molt ràpida. Anem a treballar aquesta eina:

* Llistem totes les imatges VM disponibles:

  ```bash
  virt-builder --list
  ```

* Mostrem ara les notes d'instal·lació d'Ubuntu 18.04

  ```bash
  virt-builder --notes ubuntu18.04
  ```

* Quina ordre instal·la un fedora 28?

  ```bash
  virt-builder fedora-28
  ```

* Quin és el directori habitual a on es desen les _plantilles_ de les imatges baixades?

  * Es troba al home de l'usuari a una carpeta oculta anomenada *cache*.

    ```bash
    ll ./.cache/virt-builder/fedora-28.x86_64.1
    ```

* Per defecte la imatge creada serà un fitxer _*.img_. Si vull canviar el nom del fitxer, quina opció he de fer servir?

  * Fem servir l'opció `-o`.

    ```bash
    virt-builder fedora-28 -o nomquevolguem.img
    ```

* I si vull canviar el format? _qcow2_ per exemple.

  * Amb l'opció `--format`

    ```bash
    virt-builder fedora-28 --format qcow2
    ```

* Quina ordre m'instal·la un ubuntu 16.04 amb:
  * contrasenya de root *jupiter*

    ```bash
    --root-password password:jupiter
    # o amb un fitxer
    --root-password file:/tmp/rootpw
    ```

  * amb nom *ubuntu-16_04.qcow2*

    ```bash
    -o ubuntu-16_04.qcow2
    ```

  * al directori /var/lib/libvirt/images/ (tot i que virt-builder no necessita permís de root copiar en aquest directori¿?)

    ```bash
    -o /var/lib/libvirt/images/...
    ```

  * format _qcow2_

    ```bash
    --format qcow2
    ```

  * hostname _virt.example.com_

    ```bash
    --hostname virt.example.com
    ```

  * instal·lant l'escriptori gràfic _xfce4_ 

    ```bash
    --install "xfce4"
    ```

  * actualitzant tots els paquets

    ```bash
    --update
    ```

    * Ordre completa:

    ```bash
    virt-builder ubuntu-16.04 --format qcow2 -o /var/lib/libvirt/images/ubuntu-16_04.qcow2 --root-password password:jupiter --hostname virt.example.com --install "xfce4" --update
    ```

##### Exercici 3

Creem amb virt-manager una VM a patir de la imatge Centos minimal de tipus qcow2 que es troba a `/home/groups/inf/public/install/VM/`

------

*Tipus de xarxes a les VM*

##### Exercici 4

A partir de qualsevol de les VM instal·lades als exercicis anteriors, quina és la configuració (el tipus) de xarxa per defecte?

* Per defecte el tipus de xarxa és NAT.

Quina IP té l'amfitrió del convidat

* `192.168.3.8`

Quin *default gateway* té el convidat

* `192.168.122.1`

Feu ping des de l'amfitrió al convidat:

* Es poden veure.

Feu ping des del convidat a l’amfitrió

* Es poden veure.

Feu ping des del convidat a un ordinador de la xarxa de classe

* Es poden veure.

Feu ping des del convidat a internet (se suposa que hi ha internet des de l'amfitrió)

* Té sortida a internet.

Feu ping des d'un altre ordinador de la xarxa de classe a l'ordinador convidat 

* En aquest cas l'altre ordinador no veu la màquina virtual.


##### Exercici 5

Canvieu ara la xarxa per defecte que hi ha a una VM i feu servir una xarxa bridge amb *macvtap*.

Quina IP té l'amfitrió del convidat

* `192.168.5.106`

Quin *default gateway* té el convidat

* `192.168.0.1`

Feu ping des de l'amfitrió al convidat:

* No es pot fer ping

Feu ping des del convidat a l’amfitrió

* No es pot fer ping

Feu ping des del convidat a un ordinador de la xarxa de classe

* Es pot fer ping.

Feu ping des del convidat a internet (se suposa que hi ha internet des de l'amfitrió)

* Té sortida a internet.

Feu ping des d'un altre ordinador de la xarxa de classe a l'ordinador convidat

* Es pot fer ping.


##### Exercici 6

Creeu una xarxa virtual *isolated* `192.168.150.0/24` amb DHCP i configureu dues màquines (clonant una abans si és necessari) responeu a les mateixes preguntes:

Quina IP té l'amfitrió del convidat

* `192.168.150.2`

Quin *default gateway* té el convidat

* No en té ja que és una xarxa isolated.

Feu ping des de l'amfitrió al convidat:

* No es pot fer ping.

Feu ping des del convidat a l’amfitrió

* No es pot fer ping.

Feu ping des del convidat a un ordinador de la xarxa de classe

* No es pot fer ping.

Feu ping des del convidat a internet (se suposa que hi ha internet des de l'amfitrió)

* No té sortida a internet.

Feu ping des d'un altre ordinador de la xarxa de classe a l'ordinador convidat

* No es pot fer ping.

##### Exercici 7 (virsh)

* Com podem veure amb l'ordre `virsh` totes les xarxes virtuals que gestiona libvirtd? Opció interactiva i no interactiva.

  ```bash
  [admin@localhost]virsh
  virsh # net-list
  #
  virsh net-list
  ```

* Com desactivem una xarxa?

  ```bash
  virsh net-destroy --network nomxarxa
  ```

* Com llistem totes les xarxes, tant actives com inactives?

  ```bash
  virsh net-list --all
  ```

* Com activem una xarxa inactiva?

  ```bash
  virsh net-start nomxarxa
  ```

  