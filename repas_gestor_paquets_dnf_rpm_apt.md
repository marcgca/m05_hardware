# Distribucions Red Hat (Fedora, CentOS, Red Hat Enterprise Linux)

Gestors de paquets `.rpm`

## Gestor de paquets dnf

* Llistar tots els paquets instal·lats amb `dnf list installed`

  ```bash
  $ dnf list installed
  Installed Packages
  GConf2.x86_64                        3.2.6-16.fc24              @koji-override-0
  GeoIP.x86_64                         1.6.11-1.fc24              @updates     
  GeoIP-GeoLite-data.noarch            2017.07-1.fc24             @updates     
  ImageMagick.x86_64                   6.9.3.0-2.fc24             @fedora     
  ImageMagick-c++.x86_64               6.9.3.0-2.fc24             @fedora     
  ImageMagick-libs.x86_64              6.9.3.0-2.fc24             @fedora     
  LibRaw.x86_64                        0.17.2-1.fc24              @koji-override-0
  ModemManager.x86_64                  1.6.4-1.fc24               @updates     
  ModemManager-glib.x86_64             1.6.4-1.fc24               @updates     
  NetworkManager.x86_64                1:1.2.6-1.fc24             @updates 
  ```

* Mostrar a quin paquet pertany una ordre `provides`

  ```bash
  $ dnf provides */useradd
  Last metadata expiration check: 6 days, 22:10:09 ago on Sun Jan  7 23:31:33 2018.
  shadow-utils-2:4.2.1-8.fc24.x86_64 : Utilities for managing accounts and shadow password files
  Repo        : @System # Aquest no és un repositori, sol indica que està instal·lat al sistema
  shadow-utils-2:4.2.1-8.fc24.x86_64 : Utilities for managing accounts and shadow password files
  Repo        : fedora
  ```

* Instal·lem un paquet amb `install`paquet

  ```bash
  $ dnf install mc
  ```

* Si volem tornar a instal·lar un paquet, ho fem amb `reinstall`

  ```bash
  $ dnf reinstall mc
  ```

* Busquem tots els paquets que continguin una cadena amb `search`

  ```bash
  $ dnf search libreoffice
  $ dnf search libreoffice | grep libreoffice-langpack-ca # Per trobar el paquet en català del libreoffice
  ```

* Per actualitzar un paquet ho fem amb `upgrade paquet`

  ```bash
  $ dnf upgrade libreoffice # Per actualitzar un paquet específic
  $ dnf upgrade # Per actualitzar tot el sistema
  ```

* Els grups són agrupacions de diversos paquets per a una funcionalitat específica, per exemple virtualització. Hi han diferents formes d'instal·lar aquests grups:

  * `@grup`: S'especifica que és un grup amb una arroba al davant

  * `group install grup`: Instal·lació del grup especificat amb `group` e `install` separat.

  * `groupinstall grup`: Instal·lació del grup especificat amb `groupinstall` junt.

    ```bash
    $ dnf install @Virtualization
    $ dnf group install Virtualization
    $ dnf groupinstall Virtualization
    ```

* Llistem tots els grups de paquets amb `group list`

  ```bash
  $ dnf group list
  ```

* Per llistar tots els repositoris configurats `repolist`

  ```bash
  $ dnf repolist
  ```

  * Per defecte `repolist`sol llista els repositoris que estàn habilitats, podem especificar si volem els habilitats, deshabilitats o tots:

    ```bash
    $ dnf repolist enabled # Llista els repositoris habilitats, opció per defecte.
    $ dnf repolist disabled # Llista els repositoris deshabilitats.
    $ dnf repolist all # Llista tots els repositoris.
    ```

* Podem instal·lar repositoris que no hi siguin per defecte

  ```bash
  $ dnf copr enable taw/Riot
  ```

  * En realitat el que fem amb això és crear el fitxer d'extensió _.repo_ corresponent al directori `/etc/yum.repos.d` . Allà indica la direcció url i si està habilitat o no amb `enabled=0/1`

* Podem habilitat o deshabilitar amb `config-manager --set-enabled/disabled`

  ```bash
  $ dnf config-manager --set-enabled taw-Riot # Habilitem el repositori
  $ dnf config-manager --set-disabled taw-Riot # Deshabilita el repositori
  ```

  

## Gestor de paquets rpm

* Llistem tots els paquets instal·lats

  ```bash
  $ rpm -qa
  ```

* Rpm, al contrari que dnf, descarrega i instal·la a partir d'una url, però se li ha d'especificar. Si no la sabem, podem descarregar-ho amb dnf e instal·lar-ho amb rpm.

  ```bash
  $ dnf download tftp-server # Descarreguem amb dnf
  $ rpm -ivh tftp-server-5...rpm # Instal·lem amb rpm
  ```

* Podem llistar els components d'un paquet amb `-ql`

  * Si tenim el paquet instal·lat només indiquem el nom

    ```bash
    $ rpm -ql tftp-server
    ```

  * Si no el tenim instal·lat però tenim el paquet descarregat ho fem amb `-qlp`

    ```bash
    $ rpm -qlp tftp-server-5...rpm # Nom del paquet complet
    ```

* Podem llistar els fitxers de documentació d'un paquet amb `-qd`

  ```bash
  $ rpm -qd tftp-server
  /usr/share/doc/tftp-server/CHANGES
  /usr/share/doc/tftp-server/README
  /usr/share/doc/tftp-server/README.security
  /usr/share/man/man8/in.tftpd.8.gz
  /usr/share/man/man8/tftpd.8.gz
  ```

* Podem llistar els fitxers de configuració d'un paquet amb `-qc` (si els té)

  ```bash
  $ rpm -qc tftp-server
  ```

* Si volguèssim llistar els fitxers executables (bin) no ho podem fer directament, però podem llistar el contingut i fer un grep

  ```bash
  $ rpm -ql | grep bin
  ```

* Podem mostrar la informació d'un paquet amb `-qi`

  ```bash
  $ rpm -qi tftp-server
  ```

* Podem llistar les dependències d'un paquet amb `--requires`. Per *dependències* entenem tot el necessari per funcionar, no sol paquets.

  ```bash
  $ rpm -qp --requires dhcp-client-4.3...rpm # Què necessita el paquet especificat
  /bin/bash
  /bin/sh
  coreutils
  dhcp-common = 12:4.3.4-4.fc24
  dhcp-libs(x86-64) = 12:4.3.4-4.fc24
  gawk
  grep
  ipcalc
  iproute
  iputils
  libc.so.6()(64bit)
  ```

  * Si només volem sapiguer quins paquets fa servir ho fem amb `--whatrequires`

    ```bash
    $ rpm -q --whatrequires dhclient # Quins paquets necessita el programa instal·lat
    NetworkManager-1.2.6-1.fc24.x86_64
    dracut-network-044-21.fc24.x86_64
    ```

* Podem llistar les capabilitats del paquet amb `--provides`

  ```bash
  $ rpm -qp --provides dhcp-client-4....rpm
  ```

* Podem llistar els scripts preinstal·lació i postinstal·lació d'un servei

  ```bash
  $ rpm -q --scripts tftp-server
  ```

* Per actualitzar un paquet ho fem amb `-U`

  ```bash
  $ rpm -U mc...rpm
  ```

* Per eliminar un paquet ho fem amb `-e`

  ```bash
  $ rpm -evh mc
  ```

* Podem regenerar la base de dades del rpm amb `--rebuild`

  ```bash
  $ rpm --rebuild
  ```

  